#!/usr/bin/env bash
# Script to generate csv files from hippocampal subfields
# Usage: fs_stats_hipposf.sh <subject id(s)>
# Wild card can be used.
# 08 Mar 2020 K.Nemoto

#set -x

timestamp=$(date +%Y%m%d_%H%M)


#Check if fsid(s) are specified
if [ $# -lt 1 ] ; then
  echo "Please specify fsid(s) you want to extract ROI!"
  echo "Usage: $0 <fsid(s)>"
  exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done


cd $SUBJECTS_DIR


#Check if tables directory exists in $SUBJECTS_DIR
if [ ! -d tables ]; then
  mkdir tables
fi


#temp files
temp1=$(mktemp)
temp2=$(mktemp)


#Prepare a list with subregion names
cat << EOS > tmp.subregion
Subregion
Hippocampal_tail
subiculum
CA1
hippocampal-fissure
presubiculum
parasubiculum
molecular_layer_HP
GC-ML-DG
CA3
CA4
fimbria
HATA
Whole_hippocampus
EOS

#Prepare left and right list
sed 's/^/lh_/' tmp.subregion > tmp.lt.subregion
sed 's/^/rh_/' tmp.subregion > tmp.rt.subregion

for fsid in "$@"
do
    #Check if lh.hippoSfVolumes-T1.v10.txt exist
    if [ ! -e ${fsid}/mri/lh.hippoSfVolumes-T1.v10.txt ]; then
      echo "lh.hippoSfVolumes-T1.v10.txt for ${fsid} is not found!"
      echo "Please run fs_autohipposf.sh first for ${fsid}"
    else
      echo "Subregion $fsid" > tmp.$fsid
      cat tmp.$fsid ${fsid}/mri/lh.hippoSfVolumes-T1.v10.txt | \
        awk '{ print $2 }' > tmp.lh.$fsid
      cat tmp.$fsid ${fsid}/mri/rh.hippoSfVolumes-T1.v10.txt | \
        awk '{ print $2 }' > tmp.rh.$fsid
    fi
done


#paste subegion with stats files
paste tmp.lt.subregion tmp.lh.* > tmp.lh.hippo
paste tmp.rt.subregion tmp.rh.* > tmp.rh.hippo

cat tmp.lh.hippo | sed -e 's/ /,/g' -e 's/\t/,/g' \
  -e 's/,,/,/g' > $temp1
cat tmp.rh.hippo | sed -e 's/ /,/g' -e 's/\t/,/g' \
  -e 's/,,/,/g' > $temp2

cat $temp1 $temp2 | sed -e 's/lh_Subregion/Subregion/' \
  -e '15d' > tables/${timestamp}.hipposf.trans.csv


#remove temporary files
rm tmp.* 

echo "Subregion tables were generated! Please check the tables directory"

exit

