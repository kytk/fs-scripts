#!/usr/bin/env bash
# For-loop for segmentThalamicNuclei
# Requirements: recon-all should be finished.
# Usage: fs_autoTN.sh <fsid(s)>
# Wildcard can be used.
# 11 Oct 2020 K.Nemoto

#Check if fsid(s) are specified
if [ $# -lt 1 ]
then
	echo "Please specify fsid to specify subjects!"
	echo "Usage: $0 <fsid(s)>"
	exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done


#for loop for segmentThalamicNuclei.sh
for f in "$@"
do
  fsid=${f%/}
  echo "Begin brainstem segmentation of $fsid"
  segmentThalamicNuclei.sh $fsid
done

echo "ThalamicNuclei segmentation finished!"

exit

