#!/usr/bin/env bash
# Script for QC of longitudinal data with FreeView
# Usage: fs_qc_long.sh <base fsid>
# 11 Feb 2020 K.Nemoto

if [ $# -lt 1 ]; then
  echo "Please specify base fsid!"
  echo "Usage: $0 <fsid> (e.g. bert)"
  exit 1
fi 


#Display $SUBJECTS_DIR
echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"

cd $SUBJECTS_DIR

#Set fsid from argument
fsid=${1%/}


# Obtain index for tp1 and tp2
echo "Please specify index for time points"
echo "Example: If the fsids comprise of Subj1_tp1 and Subj_tp2"
echo "index for timepoint 1 is 'tp1' and 'tp2' for timepoint 2"
read -p "Index for timepoint 1: " tp1
read -p "Index for timepoint 2: " tp2


#Check norm.mgz exists
if [ ! -f $SUBJECTS_DIR/$fsid/mri/norm.mgz ]; then
  echo "norm.mgz does not exist!"
  echo "It seems recon-all exited with error!"
  exit 1
fi

freeview \
 -v ${fsid}_${tp1}.long.${fsid}/mri/norm.mgz \
    ${fsid}_${tp2}.long.${fsid}/mri/norm.mgz \
 -f ${fsid}_${tp1}.long.${fsid}/surf/lh.pial:edgecolor=red \
    ${fsid}_${tp1}.long.${fsid}/surf/rh.pial:edgecolor=red \
    ${fsid}_${tp1}.long.${fsid}/surf/lh.white:edgecolor=blue \
    ${fsid}_${tp1}.long.${fsid}/surf/rh.white:edgecolor=blue \
 -f ${fsid}_${tp2}.long.${fsid}/surf/lh.pial:edgecolor=pink \
    ${fsid}_${tp2}.long.${fsid}/surf/rh.pial:edgecolor=pink \
    ${fsid}_${tp2}.long.${fsid}/surf/lh.white:edgecolor=lightblue \
    ${fsid}_${tp2}.long.${fsid}/surf/rh.white:edgecolor=lightblue \
 --layout 1 --viewport coronal --zoom 2 

 
