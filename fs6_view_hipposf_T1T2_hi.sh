#!/usr/bin/env bash
# Script to view Hippocampal subfields using T1 and T2 with high resolution
# Usage: fs_view_hipposf_T1T2_hi.sh <fsid>
# 11 Feb 2020 K.Nemoto

if [ $# -lt 1 ]; then
  echo "Please specify fsid!"
  echo "Usage: $0 <fsid> (e.g. bert)"
  exit 1
fi 

fsid=${1%/}

#Display $SUBJECTS_DIR
echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"

#Check if lh.hippoSfLabels-T1-${fsid}.v10.mgz exists
if [ ! -f $SUBJECTS_DIR/$fsid/mri/lh.hippoSfLabels-T1-${fsid}.v10.mgz ]; then
  echo "hippoSfLabels-T1-T2 does not exist under $SUBJECTS_DIR/$fsid !"
  echo "Please check SUBJECTS_DIR !"
  exit 1
fi

freeview -v \
  $SUBJECTS_DIR/$fsid/mri/nu.mgz \
  $SUBJECTS_DIR/$fsid/mri/lh.hippoSfLabels-T1-${fsid}.v10.mgz:colormap=lut \
  $SUBJECTS_DIR/$fsid/mri/rh.hippoSfLabels-T1-${fsid}.v10.mgz:colormap=lut \
  --layout 1 --viewport coronal --zoom 3 &


