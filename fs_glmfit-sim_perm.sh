#!/usr/bin/env bash
# execute mri_glmfit-sim --perm
# Usage: fs_glmfit-sim.sh <glmdir> <permutation> <Cluster-defining threshold> <threads>
# 29 Jan 2024 K.Nemoto

#Check if the files are specified
if [ $# -lt 4 ]
then
  echo "Please specify glmdir and cluster-defining threshold (CDT)!"
  echo "Usage: $0 <glmdir> <permutation> <CDT> <threads>"
  echo "example: mri_glmfit-sim_perm.sh <glmdir> 5000 3 10"
  exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

glmdir=${1%/}
perm=$2
cdt=$3
thread=$4


#Confirm fsgd and contrast before proceeding
while true
do
  echo "glmdir is ${glmdir}, CDT is ${cdt}, and thread is ${thread}."
  echo "Are they correct (yes/no)?"

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

#mri_glmfit-sim
for side in pos neg abs
do
  mri_glmfit-sim --glmdir ${glmdir} \
    --perm ${perm} ${cdt} ${side} \
    --cwp 0.05 --2spaces --bg ${thread}
done

exit

