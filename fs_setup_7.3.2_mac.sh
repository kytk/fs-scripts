#!/usr/bin/env bash
#Script to install freesurfer on macOS
#This script downloads required files, install them, and configure that
#subject directory is under $HOME

# 09 Nov 2022 K. Nemoto

echo "Begin installation of FreeSurfer"
echo
echo "This script will download and install Freesurfer on macOS"
echo "You need to prepare license.txt beforehand."
echo "license.txt should be placed in $HOME/Downloads"

##VERSION
ver=7.3.2
md5hash=4835d3819dcca07338c9a5488d1a07c4

while true; do

echo "Are you sure you want to begin the installation of FreeSurfer? (yes/no)"
read answer 
    case $answer in
        [Yy]*)
          echo "Begin installation."
	  break
          ;;
        [Nn]*)
          echo "Abort installation."
          exit 1
          ;;
        *)
          echo -e "Please type yes or no. \n"
          ;;
    esac
done

##Homebrew
echo "Check if homebrew is installed"

which brew > /dev/null
if [ "$?" -ne 0 ]; then
  echo "install homebrew"
  bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
else
  echo "update homebrew"
  brew update
fi

## Xquartz
echo "install xquartz"
brew install xquartz --cask

## Check license
echo "Check if you have license.txt in $HOME/Downloads"

if [ -e $HOME/Downloads/license.txt ]; then
    echo "license.txt exists. Continue installation."
else
    echo "You need to prepare license.txt"
    echo "Abort installation. Please run the script after you put license.txt in $HOME/Downloads"
fi

## Download freesurfer
if [ ! -e $HOME/Downloads/freesurfer-darwin-macOS-${ver}.tar.gz ]; then
    echo "Download Freesurfer to $HOME/Downloads"
    cd $HOME/Downloads
    curl -O https://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/${ver}/freesurfer-darwin-macOS-${ver}.tar.gz
else
    echo "Freesurfer archive is found in $HOME/Downloads"
fi

## Check the archive
cd $HOME/Downloads
echo "Check if the downloaded archive is not corrupt."
echo "MD5(freesurfer-darwin-macOS-${ver}.tar.gz)= ${md5hash}" > freesurfer-darwin-macOS-${ver}.tar.gz.md5
openssl md5 freesurfer-darwin-macOS-${ver}.tar.gz | cmp freesurfer-darwin-macOS-${ver}.tar.gz.md5 -

while [ "$?" -ne 0 ]
do
    echo "Filesize is not correct. Re-try downloading."
    curl -O https://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/${ver}/freesurfer-darwin-macOS-${ver}.tar.gz
    openssl md5 freesurfer-darwin-macOS-${ver}.tar.gz | cmp freesurfer-darwin-macOS-${ver}.tar.gz.md5 -
done

echo "Filesize is correct!"
rm freesurfer-darwin-macOS-${ver}.tar.gz.md5

## Install freesurfer
echo "Install freesurfer"
if [ ! -d /usr/local/freesurfer ]; then
  sudo mkdir /usr/local/freesurfer
fi

cd /usr/local/freesurfer
sudo tar xvzf $HOME/Downloads/freesurfer-darwin-macOS-${ver}.tar.gz    
sudo mv freesurfer ${ver}

## Prepare freesurfer directory in $HOME
echo "make freesurfer directory in $HOME"
cd $HOME
if [ ! -d freesurfer/${ver} ]; then
    mkdir -p freesurfer/${ver}
fi

cp -r /usr/local/freesurfer/${ver}/subjects $HOME/freesurfer/${ver}

## Copy license.txt 
if [ ! -e "$HOME/freesurfer/${ver}/license.txt" ]; then
    cp $HOME/Downloads/license.txt $HOME/freesurfer/${ver}/
fi

## Append setting to .bash_profile
cat $HOME/.bash_profile | grep "freesurfer/${ver}" 
if [ "$?" -eq 0 ]; then
    echo ".bash_profile is already set."
else
    echo >> $HOME/.bash_profile
    echo "#FreeSurfer ${ver}" >> $HOME/.bash_profile
    echo "export SUBJECTS_DIR=$HOME/freesurfer/${ver}/subjects" >> $HOME/.bash_profile
    echo "export FREESURFER_HOME=/usr/local/freesurfer/${ver}" >> $HOME/.bash_profile
    echo "export FS_LICENSE=$HOME/freesurfer/${ver}/license.txt" >> $HOME/.bash_profile
    echo 'source $FREESURFER_HOME/SetUpFreeSurfer.sh' >> $HOME/.bash_profile
fi

echo "Installation finished!"
echo "Now close this terminal, open another terminal, then run freeview."

### Test installation
#source $HOME/.bash_profile
#
#while true; do
#
#echo "Do you want to check if installation is done correctly? (yes/no)"
#read answer 
#    case $answer in
#        [Yy]*)
#          echo "Freeview will run shortly."
#	  break
#          ;;
#        [Nn]*)
#          echo "Finished installation."
#          exit
#          ;;
#        *)
#          echo -e "Please type yes or no. \n"
#          ;;
#    esac
#done
#
#
#cd $SUBJECTS_DIR
#freeview -v \
#    bert/mri/T1.mgz                            \
#    bert/mri/wm.mgz                            \
#    bert/mri/brainmask.mgz                     \
#    bert/mri/aseg.mgz:colormap=lut:opacity=0.2 \
#    -f                                         \
#    bert/surf/lh.white:edgecolor=blue          \
#    bert/surf/lh.pial:edgecolor=red            \
#    bert/surf/rh.white:edgecolor=blue          \
#    bert/surf/rh.pial:edgecolor=red &

exit

