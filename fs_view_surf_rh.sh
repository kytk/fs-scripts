#!/usr/bin/env bash
#A script to view rh surface data with FreeView
#Usage: fs_view_surf_rh.sh <fsid>
# 11 Feb 2020 K.Nemoto

if [ $# -lt 1 ]; then
  echo "Please specify fsid!"
  echo "Usage: $0 subject (e.g. bert)"
  exit 1
fi 


#Display $SUBJECTS_DIR
echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"

#Set fsid from argument
fsid=${1%/}


#Check if rh.pial exists
if [ ! -f $SUBJECTS_DIR/$fsid/surf/rh.pial ]; then
  echo "rh.pial does not exist under $SUBJECTS_DIR/$fsid !"
  echo "Please check SUBJECTS_DIR !"
  exit 1
fi

freeview -f \
  $SUBJECTS_DIR/$fsid/surf/rh.pial:annot=aparc.annot:name=rh.pial_aparc:visible=0 \
  $SUBJECTS_DIR/$fsid/surf/rh.inflated:overlay=rh.thickness:overlay_threshold=0.1,3::name=rh.inflated_thickness:visible=0 \
  $SUBJECTS_DIR/$fsid/surf/rh.inflated:visible=0 \
  $SUBJECTS_DIR/$fsid/surf/rh.white:visible=0 \
  $SUBJECTS_DIR/$fsid/surf/rh.pial \
  --layout 1 --viewport 3d &

