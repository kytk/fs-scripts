#!/usr/bin/env bash
# Script to visualize thickness symmetrized percent change with FreeView
# Usage: fs_thick-spc.sh <base fsid>
# 12 Nov 2020 K.Nemoto

if [ $# -lt 1 ]; then
  echo "Please specify base fsid!"
  echo "Usage: $0 <fsid> (e.g. bert)"
  exit 1
fi 

#Display $SUBJECTS_DIR
echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"

#Set fsid from argument
fsid=${1%/}


#Check lh.pial exists
if [ ! -f $SUBJECTS_DIR/$fsid/surf/lh.pial ]; then
  echo "lh.pial does not exist under $SUBJECTS_DIR/$fsid !"
  echo "Please check SUBJECTS_DIR !"
  exit 1
fi


freeview -f \
 $SUBJECTS_DIR/fsaverage/surf/lh.pial:overlay=$SUBJECTS_DIR/${fsid}/surf/lh.long.thickness-spc.fwhm15.fsaverage.mgh:overlay_threshold=2,5 \
 $SUBJECTS_DIR/fsaverage/surf/rh.pial:overlay=$SUBJECTS_DIR/${fsid}/surf/rh.long.thickness-spc.fwhm15.fsaverage.mgh:overlay_threshold=2,5 \
 --colorscale --layout 1 --viewport 3d --zoom 1.5 &


