#!/usr/bin/env bash
# fs_genroi.sh
# generate rois from atlas with .mgz extension
# Usage: fs_genroi.sh <atlas> <output_base>
# Prerequisite: fsl and freesurfer
# 08 Dec 2022  K. Nemoto

#set -x

if [[ $# -ne 3 ]]; then
  echo "Please specify the segmentation file (e.g. aseg.mgz) "
  echo "you want to extract ROIs, target file you want to resample,"
  echo "and output basename!"
  echo "Usage: $0 <segmentation (e.g. aseg.mgz)> <target> <output_base>"
  exit 1
fi

# segmentation
seg=${1%.mgz}

# target
target=$2

# orientation of target
orient=$(mri_info $target | grep Orientation | awk '{ print $3 }')

# output basename
outbase=$3

echo "Convert $1 to nifti in $target space."
mri_label2vol --seg $1 \
  --temp $target \
  --o tmp.mgz \
  --regheader $1
mri_convert tmp.mgz ${seg}.nii.gz --out_orientation $orient
rm tmp.mgz
input=${seg}.nii.gz

# make directory with basename
[[ ! -d $outbase ]] && mkdir $outbase
mv $input $outbase

# copy FreeSurferColorLUT.txt
echo "copy FreeSurferColorLUT.txt"
cp $FREESURFER_HOME/FreeSurferColorLUT.txt $outbase

cd $outbase

# Check how many regions atlas has
numroi=$(fslstats $input -R | awk '{ print int($2) }')

echo "generate rois under $outbase "

for f in $(seq -w $numroi)
do 
  lthr=$(echo "$f - 0.5" | bc)
  uthr=$(echo "$f + 0.5" | bc)
  fslmaths $input -thr $lthr -uthr $uthr -bin ${outbase}_${f}
  signal=$(fslstats ${outbase}_${f} -M | cut -c 1)
  [[ $signal == 0 ]] && rm ${outbase}_${f}.nii.gz
done

echo "Done."
echo "The number of ROI corresponds to index found in FreeSurferColorLUT.txt"

