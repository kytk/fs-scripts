#!/usr/bin/env bash
# Script to see how recon-all generates files from sample-00{1,2}.mgz
# K.Nemoto 8 Aug 2021

cd $SUBJECTS_DIR

if [ ! -d progress ]; then
  mkdir progress
fi

#arguments
cat << EOS > recon_directives
01 -motioncor		Motin_Correct_and_Average
02 -talairach		Talairach
03 -normalization	Intensity_Normalization1
04 -skullstrip		Skull_Stripping
05 -nuintensitycor	Nu_Intensity_Correction
06 -subcortseg		Automatic_Subcortical_Segmentation
07 -gcareg		GCA_Registration
08 -canorm		Canonical_Intensity_Normalization
09 -careg		Canonical_Registration
10 -rmneck		Remove_Neck
11 -skull-lta		EM_Registration_with_Skull
12 -calabel		SubCort_Segmentation
13 -segstats		Aseg_Stats
14 -normalization2	Intensity_Normalization2
15 -segmentation	WM_Segmentation	
16 -fill		Fill
17 -tessellate		Tessellate
18 -smooth1		Smooth1
19 -inflate1		Inflate1
20 -qsphere		Qsphere
21 -fix			Fix_Topology
22 -smooth2		Smooth2
23 -inflate2		Inflate2
24 -white		Make_White_Surfaces
25 -sphere		Spherical_Inflation 
26 -surfreg		Ipsilateral_Surface_Registration
27 -jacobian_white	Jacobian_White
28 -jacobian_dist0	Surface Registation,_maximal_distortion,_with_Jacobian
29 -contrasurfreg	Contralateral_Surface_Registration
30 -avgcurv		Average_Curv_for_Display
31 -cortparc		Cortical_Parcellation
32 -cortparc2		Cortical_Parcellation_2
33 -cortparc3		Cortical_Parcellation_3
34 -pial		Make_Pial_Surfaces
35 -pctsurfcon		WM/GM_Contrast
36 -surfvolume		Surface_Volume
37 -cortribbon		Cortical_Ribbon
38 -parcstats		Surface_Anatomical_Stats
39 -parcstats2		Surface_Anatomical_Stats_2
40 -parcstats3		Surface_Anatomical_Stats_3
41 -curvstats		Curvature_Anatomical_Stats
EOS

#prepare fsid and images
echo "=============================="
echo "Begin #00"
source fs_timestamp.sh
recon-all -s ernie -i sample-001.mgz -i sample-002.mgz 
echo "Step00 Prepare fsid and images" > progress/step_00_result.txt
find ernie -type f -newer /tmp/_timestamp | tee -a progress/step_00_result.txt
echo "End #00"
echo "=============================="

#directives
cat recon_directives | while read no directive descrip
do
  echo "=============================="
  echo "Begin #${directive}"
  source fs_timestamp.sh
  result=step_${no}_result.txt
  recon-all -s ernie $directive
  echo "Step_${no} $directive $descrip" > progress/${result}
  find ernie -type f -newer /tmp/_timestamp | tee -a progress/${result}
  echo "End #${directive}"
  echo "=============================="
done

rm recon_directives
rm /tmp/_timestamp

