#!/usr/bin/env bash
# For-loop for recon-all brainstem
# Requirements: recon-all should be finished.
# Usage: fs6_autobrainstem.sh <fsid(s)>
# Wildcard can be used.
# 11 Feb 2020 K.Nemoto

#Check if fsid(s) are specified
if [ $# -lt 1 ]
then
	echo "Please specify fsid to specify subjects!"
	echo "Usage: $0 <fsid(s)>"
	exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done


#recon-all -brainstem-structures
for f in "$@"
do
  fsid=${f%/}
  echo "Begin brainstem segmentation of $fsid"
  recon-all -s $fsid -brainstem-structures
done

echo "Brainstem segmentation finished!"

exit

