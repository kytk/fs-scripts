#!/usr/bin/env bash
# For-loop for recon-all hippocampal subfields with only T1
# Requirements: recon-all should be finished.
# Usage: fs6_autohipposf.sh <fsid(s)>
# Wildcard can be used.
# 11 Feb 2020 K.Nemoto

#Check if the fsid(s) are specified
if [ $# -lt 1 ]
then
	echo "Please specify fsid(s) to specify subjects!"
	echo "Usage: $0 <fsid(s)>"
	exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done


#recon-all -hippocampal-subfields-T1
for f in "$@"
do
  fsid=${f%/}
  echo "Begin hippocampal subfield segmentation of $fsid"
  recon-all -s $fsid -hippocampal-subfields-T1
done

echo "Hippocampal subfield segmentation finished!"

exit

