#!/usr/bin/env bash
# Script to set Matlab R2019b Runtime for hippocampal subfield calculation
# This script is based on 
# https://surfer.nmr.mgh.harvard.edu/fswiki/MatlabRuntime
# 22 Oct 2022 K.Nemoto

sudo FREESURFER_HOME=$FREESURFER_HOME $FREESURFER_HOME/bin/fs_install_mcr R2019b

exit

