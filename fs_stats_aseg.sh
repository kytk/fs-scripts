#!/usr/bin/env bash
# Script to utilize asegstats2table
# CSV file will be generated.
# Usage: fs_stats_aseg.sh <subject_id(s)>
# Wild card can be used.
# 07 Nov 2020 K.Nemoto

#set -x

timestamp=$(date +%Y%m%d_%H%M)

#Check if fsid(s) are specified
if [ $# -lt 1 ] ; then
  echo "Please specify fsid(s) you want to extract ROI!"
  echo "Usage: $0 <fsid(s)>"
  exit 1
fi

#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

cd $SUBJECTS_DIR

#Check if tables directory exists in $SUBJECTS_DIR
if [ ! -d tables ]; then
  mkdir tables
fi
cd tables

asegstats2table --subjects "$@"     \
                --delimiter comma   \
                --table ${timestamp}.aseg.csv

cd $SUBJECTS_DIR

echo "${timestamp}.aseg.csv was generated! Please check 'tables' directory"

exit

