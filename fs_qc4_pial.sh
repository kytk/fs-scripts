#!/usr/bin/env bash
#A script for QC of pial surface with FreeView
#Usage: fs_qc4_pial.sh <Subject>

if [ $# -lt 1 ]; then
  echo "Please specify fsid!"
  echo "Usage: $0 <fsid> (e.g. bert)"
  exit 1
fi 


fsid=${1%/}


#Check brainmask.mgz exists
if [ ! -f $SUBJECTS_DIR/$fsid/mri/brainmask.mgz ]; then
  echo "brainmask.mgz does not exist!"
  echo "It seems recon-all exited with error!"
  exit 1
fi

freeview -v \
  $SUBJECTS_DIR/$fsid/mri/T1.mgz \
  $SUBJECTS_DIR/$fsid/mri/brainmask.mgz \
  -f  \
  $SUBJECTS_DIR/$fsid/surf/lh.white:edgecolor=yellow \
  $SUBJECTS_DIR/$fsid/surf/lh.pial:edgecolor=red \
  $SUBJECTS_DIR/$fsid/surf/rh.white:edgecolor=yellow \
  $SUBJECTS_DIR/$fsid/surf/rh.pial:edgecolor=red \
  --layout 4 --viewport coronal &
