#!/usr/bin/env bash
#A script to view lh surface data with FreeView
#Usage: fs_view_surf_lh.sh <fsid>
# 11 Feb 2020 K.Nemoto

if [ $# -lt 1 ]; then
  echo "Please specify fsid!"
  echo "Usage: $0 <fsid> (e.g. bert)"
  exit 1
fi 


#Display $SUBJECTS_DIR
echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"

#Set fsid from argument
fsid=${1%/}


#Check lh.pial exists
if [ ! -f $SUBJECTS_DIR/$fsid/surf/lh.pial ]; then
  echo "lh.pial does not exist under $SUBJECTS_DIR/$fsid !"
  echo "Please check SUBJECTS_DIR !"
  exit 1
fi

freeview -f \
  $SUBJECTS_DIR/$fsid/surf/lh.pial:annot=aparc.annot:name=lh.pial_aparc:visible=0 \
  $SUBJECTS_DIR/$fsid/surf/lh.inflated:overlay=lh.thickness:overlay_threshold=0.1,3::name=lh.inflated_thickness:visible=0 \
  $SUBJECTS_DIR/$fsid/surf/lh.inflated:visible=0 \
  $SUBJECTS_DIR/$fsid/surf/lh.white:visible=0 \
  $SUBJECTS_DIR/$fsid/surf/lh.pial \
  --layout 1 --viewport 3d &

