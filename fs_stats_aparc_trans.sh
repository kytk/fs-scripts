#!/usr/bin/env bash
# Script to utilize aparcstats2table
# Surface, Thickness, Thickness_SD, and Volume csv files will be generated.
# Usage: fs_stats_aparc_trans.sh <subject_id(s)>
# 08 Mar 2020 K.Nemoto

#set -x

timestamp=$(date +%Y%m%d_%H%M)

#Check if the fsid(s) are specified
if [ $# -lt 1 ] ; then
  echo "Please specify fsid(s) you want to extract ROI!"
  echo "Usage: $0 <fsid(s)>"
  exit 1
fi

#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

cd $SUBJECTS_DIR

#Check if tables directory exists in $SUBJECTS_DIR
if [ ! -d tables ]; then
  mkdir tables
fi
cd tables

#temp files
temp1=$(mktemp)
temp2=$(mktemp)
temp3=$(mktemp)
temp4=$(mktemp)


#Desikan-Killiany
##Area
###left hemisphere
aparcstats2table --hemi lh \
  --subjects "$@" \
  --transpose \
  --delimiter comma \
  --table $temp1

###right hemisphere
aparcstats2table --hemi rh \
  --subjects "$@" \
  --transpose \
  --delimiter comma \
  --table $temp2

###cat left and right
cat $temp1 $temp2 > $temp3
sed -e '37,39d' \
    -e 's/lh.aparc/aparc/' \
    $temp3 > ${timestamp}.DK.area.trans.csv


##DK-Thickness
###left hemisphere
aparcstats2table --hemi lh \
  --subjects "$@" \
  --meas thickness \
  --transpose \
  --delimiter comma \
  --table $temp1

###right hemisphere
aparcstats2table --hemi rh \
  --subjects "$@" \
  --meas thickness \
  --transpose \
  --delimiter comma \
  --table $temp2

####cat left and right hemisphere
cat $temp1 $temp2 > $temp3
sed -e '37,39d' \
    -e 's/lh.aparc/aparc/' \
    $temp3 > ${timestamp}.DK.thickness.trans.csv


##DK-Thicknessstd
###left hemisphere
aparcstats2table --hemi lh \
  --subjects "$@" \
  --meas thicknessstd \
  --transpose \
  --delimiter comma \
  --table $temp1

###right hemisphere
aparcstats2table --hemi rh \
  --subjects "$@" \
  --meas thicknessstd \
  --transpose \
  --delimiter comma \
  --table $temp2

####cat left and right hemisphere
cat $temp1 $temp2 > $temp3
sed -e '36,38d' \
    -e 's/lh.aparc/aparc/' \
    $temp3 > ${timestamp}.DK.thicknessstd.trans.csv


##DK-Volume
###left hemisphere
aparcstats2table --hemi lh \
  --subjects "$@" \
  --meas volume \
  --transpose \
  --delimiter comma \
  --table $temp1

###right hemisphere
aparcstats2table --hemi rh \
  --subjects "$@" \
  --meas volume \
  --transpose \
  --delimiter comma \
  --table $temp2

####cat left and right
cat $temp1 $temp2 > $temp3
sed -e '36,38d' \
    -e 's/lh.aparc/aparc/' \
    $temp3 > ${timestamp}.DK.volume.trans.csv


#Destrieux
##Area
###left hemisphere
aparcstats2table --hemi lh \
  --subjects "$@" \
  --transpose \
  --delimiter comma \
  --parc aparc.a2009s \
  --table $temp1

###right hemisphere
aparcstats2table --hemi rh \
  --subjects "$@" \
  --transpose \
  --delimiter comma \
  --parc aparc.a2009s \
  --table $temp2

####cat left and right
cat $temp1 $temp2 > $temp3
sed -e '77,79d' \
    -e 's/lh.aparc/aparc/' \
    $temp3 > ${timestamp}.Destrieux.area.trans.csv

##Destrieux-Thickness
###left hemisphere
aparcstats2table --hemi lh \
  --subjects "$@" \
  --meas thickness \
  --transpose \
  --delimiter comma \
  --parc aparc.a2009s \
  --table $temp1

###right hemisphere
aparcstats2table --hemi rh \
  --subjects "$@" \
  --meas thickness \
  --transpose \
  --delimiter comma \
  --parc aparc.a2009s \
  --table $temp2

####cat left and right
cat $temp1 $temp2 > $temp3
sed -e '77,79d' \
    -e 's/lh.aparc/aparc/' \
    $temp3 > ${timestamp}.Destrieux.thickness.trans.csv


##Destrieux-Thicknessstd
###left hemisphere
aparcstats2table --hemi lh \
  --subjects "$@" \
  --meas thicknessstd \
  --transpose \
  --delimiter comma \
  --parc aparc.a2009s \
  --table $temp1

###right hemisphere
aparcstats2table --hemi rh \
  --subjects "$@" \
  --meas thicknessstd \
  --transpose \
  --delimiter comma \
  --parc aparc.a2009s \
  --table $temp2

####cat left and right
cat $temp1 $temp2 > $temp3
sed -e '76,78d' \
    -e 's/lh.aparc/aparc/' \
    $temp3 > ${timestamp}.Destrieux.thicknessstd.trans.csv


##Destrieux-Volume
###left hemisphere
aparcstats2table --hemi lh \
  --subjects "$@" \
  --meas volume \
  --transpose \
  --delimiter comma \
  --parc aparc.a2009s \
  --table $temp1

###right hemisphere
aparcstats2table --hemi rh \
  --subjects "$@" \
  --meas volume \
  --transpose \
  --delimiter comma \
  --parc aparc.a2009s \
  --table $temp2

####cat left and right
cat $temp1 $temp2 > $temp3
sed -e '76,78d' \
    -e 's/lh.aparc/aparc/' \
    $temp3 > ${timestamp}.Destrieux.volume.trans.csv

#DKT
##Area
###left hemisphere
aparcstats2table --hemi lh \
  --subjects "$@" \
  --transpose \
  --delimiter comma \
  --parc aparc.DKTatlas \
  --table $temp1

###right hemisphere
aparcstats2table --hemi rh \
  --subjects "$@" \
  --transpose \
  --delimiter comma \
  --parc aparc.DKTatlas \
  --table $temp2

####cat left and right
cat $temp1 $temp2 > $temp3
sed -e '34,36d' \
    -e 's/lh.aparc/aparc/' \
    $temp3 > ${timestamp}.DKT.area.trans.csv

##DKT-Thickness
###left hemisphere
aparcstats2table --hemi lh \
  --subjects "$@" \
  --meas thickness \
  --transpose \
  --delimiter comma \
  --parc aparc.DKTatlas \
  --table $temp1

###right hemisphere
aparcstats2table --hemi rh \
  --subjects "$@" \
  --meas thickness \
  --transpose \
  --delimiter comma \
  --parc aparc.DKTatlas \
  --table $temp2

####cat left and right
cat $temp1 $temp2 > $temp3
sed -e '34,36d' \
    -e 's/lh.aparc/aparc/' \
    $temp3 > ${timestamp}.DKT.thickness.trans.csv


##DKT-Thicknessstd
###left hemisphere
aparcstats2table --hemi lh \
  --subjects "$@" \
  --meas thicknessstd \
  --transpose \
  --delimiter comma \
  --parc aparc.DKTatlas \
  --table $temp1

###right hemisphere
aparcstats2table --hemi rh \
  --subjects "$@" \
  --meas thicknessstd \
  --transpose \
  --delimiter comma \
  --parc aparc.DKTatlas \
  --table $temp2

####cat left and right
cat $temp1 $temp2 > $temp3
sed -e '33,35d' \
    -e 's/lh.aparc/aparc/' \
    $temp3 > ${timestamp}.DKT.thicknessstd.trans.csv


##DKT-Volume
###left hemisphere
aparcstats2table --hemi lh \
  --subjects "$@" \
  --meas volume \
  --transpose \
  --delimiter comma \
  --parc aparc.DKTatlas \
  --table $temp1

###right hemisphere
aparcstats2table --hemi rh \
  --subjects "$@" \
  --meas volume \
  --transpose \
  --delimiter comma \
  --parc aparc.DKTatlas \
  --table $temp2

####cat left and right
cat $temp1 $temp2 > $temp3
sed -e '33,35d' \
    -e 's/lh.aparc/aparc/' \
    $temp3 > ${timestamp}.DKT.volume.trans.csv

cd $SUBJECTS_DIR

echo "CSV files were generated! Please check 'tables' directory"

exit

