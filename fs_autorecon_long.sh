#!/usr/bin/env bash
# script to perform longitudinal analysis
# Usage: fs_autorecon_long.sh <fsid(s)> 
# Prerequisites: base_fsid_tp1 and base_fsid_tp2 should have been processed with recon-all beforehand.
# K.Nemoto 08 Nov 2020

#set -x

#Check if the fsid(s) are specified
if [ $# -lt 1 ]
then
  echo "Please specify fsid(s)!"
  echo "Usage: $0 <fsid(s)>"
  echo "Wild card can be used."
  exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done


#copy fsaverage and {lr}h.EC_average to $SUBJECTS_DIR if they don't exsit
find $SUBJECTS_DIR -maxdepth 1 | egrep fsaverage$ > /dev/null
if [ $? -eq 1 ]; then
  cp -r $FREESURFER_HOME/subjects/fsaverage $SUBJECTS_DIR
fi

find $SUBJECTS_DIR -maxdepth 1 | egrep [lr]h.EC_average$ > /dev/null
if [ $? -eq 1 ]; then
  cp -r $FREESURFER_HOME/subjects/[lr]h.EC_average $SUBJECTS_DIR
fi


# Obtain index for tp1 and tp2
echo "Please specify index for time points"
echo "Example: If the fsids comprise of Subj1_tp1 and Subj_tp2"
echo "index for timepoint 1 is 'tp1' and 'tp2' for timepoint 2"
read -p "Index for timepoint 1: " tp1
read -p "Index for timepoint 2: " tp2

#generate temporary files
tmp0=$(mktemp)
tmp1=$(mktemp)

# Generate subject list from arguments
echo '' > $tmp0
for f in "$@"
do
  echo $f >> $tmp0
done

cat $tmp0 | sed -e "s/_${tp1}//" -e "s/_${tp2}//" | sort | uniq | sed '/^$/d' > $tmp1 

echo "fsid_base(s) are the followings"
cat $tmp1
echo ""

# recon-all -base and -long
cat $tmp1 | while read fsid_base
do
  #recon-all -base
  recon-all -base ${fsid_base} -tp ${fsid_base}_${tp1} -tp ${fsid_base}_${tp2} -all

  #recon-all -long
  recon-all -long ${fsid_base}_${tp1} ${fsid_base} -all
  recon-all -long ${fsid_base}_${tp2} ${fsid_base} -all
done

