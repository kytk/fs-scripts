#!/usr/bin/env bash

# Usage
# fs_gen_aparc_roi_stats.sh <statistics.txt>

# You need to prepare a file which has aparc ROI number and statistics
# e.g.
# 1001 1.25
# 1002 3.16
# 1004 2.34

# Filename without extension would be the filename for the generated stat image.

# 05 Feb 2022 K.Nemoto

if [ $# -lt 1 ]; then
  echo "Please specify stat file!"
  echo "Usage: $0 <stat.txt>"
  echo "Filename of the stat file would be the filename of stat image"
  exit 1
fi 


# Apply statistics (e.g. p-value or t-value) to rois
basename=${1%.*}
echo "Apply ROI statistics to each roi"
cat $1 | while read id value
do
  fslmaths aparc_$id -mul $value stat_$id
done

# Generate base.nii.gz from aparc_1001.nii.gz
fslmaths aparc_1001 -mul 0 base

# Gather stat files and generate one stat file
fslmaths base $(ls stat_* | while read line; do echo " -add $line"; done | perl -pe 's/\n//') ${basename}

# Change to FS space
mri_vol2vol --mov ${basename}.nii.gz --targ ${SUBJECTS_DIR}/fsaverage/mri/aseg.mgz --regheader --no-save-reg --o ${basename}.mgz

# Convert to Volume data
mri_vol2surf --mov ${basename}.mgz --regheader fsaverage --o lh.${basename}.mgz --hemi lh
mri_vol2surf --mov ${basename}.mgz --regheader fsaverage --o rh.${basename}.mgz --hemi rh

# Delete base and temporary files
rm base.nii.gz stat*.nii.gz

exit

