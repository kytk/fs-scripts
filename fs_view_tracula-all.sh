#!/usr/bin/env bash
#A script to view all white-matter pathways generated by tracula
# with FreeView
#Usage: fs_view_tracula-all.sh <fsid>
#19 Feb 2020 K.Nemoto

if [ $# -lt 1 ]; then
  echo "Please specify fsid!"
  echo "Usage: $0 <fsid> (e.g. bert)"
  exit 1
fi


#Display $SUBJECTS_DIR
echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"

#Set fsid from argument
fsid=${1%/}


freeview -tv $SUBJECTS_DIR/$fsid/dpath/merged_avg33_mni_bbr.mgz \
	-v $SUBJECTS_DIR/$fsid/dmri/dtifit_FA.nii.gz \
	-layout 3 -viewport 3d -zoom 2 &

