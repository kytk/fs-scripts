#!/usr/bin/env bash

# script to generate surface image from atlas and ROI statistics
# Usage: fs_gen_roi2surface_stats.sh <atlas> <input.csv>
# K. Nemoto 07 Feb 2025

# set working directory
wd=$PWD

# Debug
#set -x

# Exit if any error occurs
set -e 

# Check arguments
if [ "$#" -ne 2 ]; then
    echo "Usage: fs_gen_roi2surface_stats.sh <atlas.nii.gz> <input.csv>"
    exit 1
fi

# Check if first argument is a NIFTI file
if [[ ! $1 =~ \.nii(\.gz)?$ ]]; then
    echo "Error: First argument must be a NIFTI file (*.nii or *.nii.gz)"
    exit 1
fi

# Check if second argument is a CSV file
if [[ ! $2 =~ \.csv$ ]]; then
    echo "Error: Second argument must be a CSV file (*.csv)"
    exit 1
fi

# Check if files exist
if [ ! -f "$1" ]; then
    echo "Error: Atlas file $1 not found"
    exit 1
fi

if [ ! -f "$2" ]; then
    echo "Error: CSV file $2 not found"
    exit 1
fi

ATLAS=$(imglob $1)
INPUT_CSV=$2

# Confirm parameters before proceeding
while true
do
    echo -e "\nParameters are the following:"
    echo "Atlas file: ${ATLAS}"
    echo "Input CSV: ${INPUT_CSV}"
    echo -e "\nWould you like to proceed with these parameters? (yes/no)"

    read answer

    case $answer in
        [Yy]*)
            break
            ;;
        [Nn]*)
            echo -e "Operation cancelled by user.\n"
            exit 1
            ;;
        *)
            echo -e "Please type yes or no.\n"
            ;;
    esac
done

echo -e "\nStep 1: Generating ROIs from atlas..."
# Create output directory
mkdir -p ${ATLAS}_rois
outdir=${ATLAS}_rois

# Check how many regions the atlas has
numroi=$(fslstats $ATLAS -R | awk '{ print int($2) }')
echo "Found $numroi regions in the atlas"

# Generate ROIs
for f in $(seq -w $numroi)
do 
    lthr=$(echo "$f - 0.5" | bc)
    uthr=$(echo "$f + 0.5" | bc)
    fslmaths $ATLAS -thr $lthr -uthr $uthr -bin ${outdir}/${ATLAS}_${f}
    signal=$(fslstats ${outdir}/${ATLAS}_${f} -M | cut -c 1)
    [[ $signal == 0 ]] && rm ${outdir}/${ATLAS}_${f}.nii.gz  
done

echo -e "\nStep 2: Processing ROIs with values..."
# Convert CSV to TSV
echo "Converting CSV to TSV..."
cat "$INPUT_CSV" | tr ',' '\t' > tmp.tsv

# Calculate min and max values for threshold
echo "Calculating thresholds..."
THRESHOLDS=$(awk 'BEGIN{min=1;max=0} {if($2<min){min=$2} if($2>max){max=$2}} END{print min","max}' tmp.tsv)
MIN_THRESH=$(echo $THRESHOLDS | cut -d',' -f1)
MAX_THRESH=$(echo $THRESHOLDS | cut -d',' -f2)
echo "Value range: $MIN_THRESH to $MAX_THRESH"

# Copy fsaverage to current directory
echo "Copy fsaverage to current directory..."
if [ ! -d fsaverage ]; then
    if [ -z "$FREESURFER_HOME" ]; then
        echo "Error: FREESURFER_HOME environment variable is not set"
        exit 1
    fi
    cp -r $FREESURFER_HOME/subjects/fsaverage .
fi

# Set SUBJECTS_DIR for mri_vol2surf --mni152reg
export SUBJECTS_DIR=$PWD

# Change to ROI directory and process
cd ${outdir}

# Multiply each ROI by its corresponding value
echo "Processing ROI regions..."
cat ../tmp.tsv | while read id value; do
    fslmaths ${ATLAS}_$(echo $id | cut -d'_' -f2) -mul $value stat_${id}
done

# Create base image
echo "Creating base image..."
fslmaths $(ls stat_*.nii.gz | head -n 1) -mul 0 tmp_base

# Create integrated image
echo "Creating integrated image..."
fslmaths tmp_base $(ls stat_* | while read line; do echo " -add $line"; done | perl -pe 's/\n//') integrated

# Convert from MNI152 space to fsaverage surface
echo "Converting to surface space..."
mri_vol2surf --mov integrated.nii.gz --mni152reg --hemi lh --o ../lh.integrated.mgz
mri_vol2surf --mov integrated.nii.gz --mni152reg --hemi rh --o ../rh.integrated.mgz

cd ${wd}

echo "Process completed successfully!"

# delete temporary files
find . -type f -name 'tmp*' -exec rm {} \;

# Display in freeview
echo "Opening freeview..."
freeview -f \
    fsaverage/surf/rh.inflated:overlay=rh.integrated.mgz:visible=0:overlay_threshold=$MIN_THRESH,$MAX_THRESH \
    fsaverage/surf/rh.pial:overlay=rh.integrated.mgz:visible=0:overlay_threshold=$MIN_THRESH,$MAX_THRESH \
    fsaverage/surf/lh.inflated:overlay=lh.integrated.mgz:visible=0:overlay_threshold=$MIN_THRESH,$MAX_THRESH \
    fsaverage/surf/lh.pial:overlay=lh.integrated.mgz:visible=1:overlay_threshold=$MIN_THRESH,$MAX_THRESH \
    --layout 1 --viewport 3d --colorscale &
