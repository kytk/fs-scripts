#!/usr/bin/env bash
# A script for QC of wm.mgz (white surface and intensity normalization)
# with FreeView
# Usage: fs_qc3_wm.sh <fsid>
# 28 Jan 2024 K.Nemoto

if [ $# -lt 1 ]; then
  echo "Please specify fsid!"
  echo "Usage: $0 <fsid> (e.g. bert)"
  exit 1
fi 


fsid=${1%/}


#Check brainmask.mgz exists
if [ ! -f $SUBJECTS_DIR/$fsid/mri/brainmask.mgz ]; then
  echo "brainmask.mgz does not exist!"
  echo "It seems recon-all exited with error!"
  exit 1
fi

freeview -v \
  $SUBJECTS_DIR/$fsid/mri/brainmask.mgz \
  $SUBJECTS_DIR/$fsid/mri/wm.mgz:colormap=heat:opacity=0.8 \
  -f  \
  $SUBJECTS_DIR/$fsid/surf/lh.white:edgecolor=yellow \
  $SUBJECTS_DIR/$fsid/surf/lh.pial:edgecolor=red \
  $SUBJECTS_DIR/$fsid/surf/rh.white:edgecolor=yellow \
  $SUBJECTS_DIR/$fsid/surf/rh.pial:edgecolor=red \
  --layout 4 --viewport coronal &

