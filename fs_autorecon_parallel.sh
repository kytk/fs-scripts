#!/usr/bin/env bash
# For-loop for recon-all with qcache in parallel
# detect number of cores and memory automatically and
# set how many recon-all will be done in parallel.

# Usage: fs_autorecon_parallel.sh <nifti file(s)>
# Wild card can be used.
# nifti file name will be the subject id for FreeSurfer
# e.g. sub01.nii -> sub01

# 03 Dec 2022 K.Nemoto

#set -x #for debugging

#Check OS
os=$(uname)

#Check number of cores (threads)
if [[ $os == "Linux" ]]; then
  ncores=$(nproc)
  mem=$(cat /proc/meminfo | grep MemTotal | awk '{ printf("%d\n",$2/1024/1024) }')
elif [[ $os == "Darwin" ]]; then 
  ncores=$(sysctl -n hw.ncpu)
  mem=$(sysctl -n hw.memsize | awk '{ print $1/1024/1024/1024 }')
else
  echo "Cannot detect your OS!"
  exit 1
fi
echo "logical cores: $ncores "
echo "memory: ${mem}GB "

#Set parameter for parallel processing
# if ncores=1, set maxrunnning=1
# if ncores>1, compare ncores and meory and 
# employ the smaller value
# set maxrunning as $ncores|$mem - 1

if [[ $ncores -eq 1 ]]; then
  maxrunning=1
elif [[ $ncores -le $mem ]]; then
  maxrunning=$(($ncores - 1))
else
  maxrunning=$(($mem - 1))
fi
echo "set maxrunning=${maxrunning}"

#Check if the files are specified
if [[ $# -lt 1 ]]
then
  echo "Please specify nifti files!"
  echo "Usage: $0 <nifti_file(s)>"
  echo "Wild card can be used."
  exit 1
fi

#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

#copy fsaverage and {lr}h.EC_average to $SUBJECTS_DIR if they don't exsit
find $SUBJECTS_DIR -maxdepth 1 | egrep fsaverage$ > /dev/null
if [[ $? -eq 1 ]]; then
  cp -r $FREESURFER_HOME/subjects/fsaverage $SUBJECTS_DIR
fi

find $SUBJECTS_DIR -maxdepth 1 | egrep [lr]h.EC_average$ > /dev/null
if [[ $? -eq 1 ]]; then
  cp -r $FREESURFER_HOME/subjects/[lr]h.EC_average $SUBJECTS_DIR
fi

#recon-all
for f in "$@"
do
  running=$(ps -aux | grep 'bin/recon-all' | wc -l)
  while [[ $running -gt $maxrunning ]];
  do
    sleep 300
    running=$(ps -aux | grep 'bin/recon-all' | wc -l)
  done
  fsid=${f%.nii*}
  recon-all -i $f -s $fsid -all -qcache &
  #recon-all -i $f -s $fsid -autorecon1 & #for debugging
done

exit

