#!/usr/bin/env bash
# Script for QC of skull-strip with FreeView
# Usage: fs_qc1_skullstrip.sh <fsid>
# 19 Jan 2019 K.Nemoto

if [ $# -lt 1 ]; then
  echo "Please specify fsid!"
  echo "Usage: $0 <fsid> (e.g. bert)"
  exit 1
fi 


fsid=${1%/}


#Check brainmask.mgz exists
if [[ ! -d $SUBJECTS_DIR/$fsid ]]; then
  echo "$fsid does not exist!"
  echo "Please check your SUBJECTS_DIR"
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  exit 1
elif [[ ! -f $SUBJECTS_DIR/$fsid/mri/brainmask.mgz ]]; then
  echo "brainmask.mgz does not exist!"
  echo "It seems recon-all exited with error!"
  exit 1
fi

freeview -v \
  $SUBJECTS_DIR/$fsid/mri/T1.mgz \
  $SUBJECTS_DIR/$fsid/mri/brainmask.mgz:colormap=heat \
  --layout 4 --viewport coronal &
 
