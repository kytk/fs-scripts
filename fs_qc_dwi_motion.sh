#!/usr/bin/env bash

#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed recon-all under this directory (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

echo "fsid AvgTranslation AvgRotation PercentBadSlices AvgDropoutScore" > $SUBJECTS_DIR/dwi_qc.tsv

for dwifile in $(find $SUBJECTS_DIR -name 'dwi_motion.txt' | sort)
do
	fsid=$(echo $dwifile | sed -e "s@$SUBJECTS_DIR/@@" -e 's@/dmri.*$@@')
	score=$(cat $dwifile | sed -n 2p)
	echo "$fsid $score" >> $SUBJECTS_DIR/dwi_qc.tsv
done

column -t $SUBJECTS_DIR/dwi_qc.tsv

echo 'QC result is saved in $SUBJECTS_DIR/dwi_qc.tsv'

exit

