#!/usr/bin/env bash
# recon-all with qcache and hippocampal subfield
# Usage: fs_autoreconT1T2.sh <fsid> <T1> <T2>
# nifti file name will be the subject id for FreeSurfer
# e.g. con001.nii -> con001
# 11 Feb 2020 K.Nemoto

#Check if the files are specified
if [ $# -lt 3 ]
then
  echo "Please specify fsid, T1, and T2 files!"
  echo "Usage: $0 <fsid> <T1> <T2>"
  exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

fsid=${1%/}
t1=$2
t2=$3


#Confirm fsid, t1, and t2 before proceeding
while true
do
  echo "fsid is ${fsid}, T1 is ${t1}, and T2 is ${t2}."
  echo "Are they correct (yes/no)?"

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

#copy fsaverage and {lr}h.EC_average to $SUBJECTS_DIR if they don't exsit
find $SUBJECTS_DIR -maxdepth 1 | egrep fsaverage$ > /dev/null
if [ $? -eq 1 ]; then
  cp -r $FREESURFER_HOME/subjects/fsaverage $SUBJECTS_DIR
fi

find $SUBJECTS_DIR -maxdepth 1 | egrep [lr]h.EC_average$ > /dev/null
if [ $? -eq 1 ]; then
  cp -r $FREESURFER_HOME/subjects/[lr]h.EC_average $SUBJECTS_DIR
fi


#recon-all
recon-all -s $fsid -i $t1 -T2 $t2 -T2pial -all -qcache -hippocampal-subfields-T1T2 $t2 $fsid

exit

