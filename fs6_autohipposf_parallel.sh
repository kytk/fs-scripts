#!/usr/bin/env bash
# For-loop for recon-all hippocampal subfields with only T1 in parallel
# Requirements: recon-all should be finished.
# Usage: fs6_autohipposf_parallel.sh <fsid(s)>
# Wildcard can be used.
# 08 Mar 2020 K.Nemoto

#Check OS
os=$(uname)

#Check number of cores (threads)
if [ $os == "Linux" ]; then
  ncores=$(nproc)
elif [ $os == "Darwin" ]; then 
  ncores=$(sysctl -n hw.ncpu)
else
  echo "Cannot detect your OS!"
  exit 1
fi

echo "Your logical cores are $ncores "

#Set parameter for parallel processing
# If $ncores >= 2, set maxrunning as $ncores - 1
# else, set maxrunning as 1

if [ $ncores -gt 1 ]; then
  maxrunning=$(($ncores - 1))
else
  maxrunning=1
fi



#Check if the fsid(s) are specified
if [ $# -lt 1 ]
then
	echo "Please specify fsid(s) to specify subjects!"
	echo "Usage: $0 <fsid(s)>"
	exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done


#recon-all -hippocampal-subfields-T1
for f in "$@"
do
  running=$(ps -aux | grep 'bin/recon-all' | wc -l)
  while [ $running -gt $maxrunning ];
  do
    sleep 60
    running=$(ps -aux | grep 'bin/recon-all' | wc -l)
  done
  fsid=${f%/}
  echo "Begin hippocampal subfield segmentation of $fsid"
  recon-all -s $fsid -hippocampal-subfields-T1 &
done

echo "Hippocampal subfield segmentation finished!"

exit

