#!/usr/bin/env bash
# For-loop for recon-all for high resolution MRI with qcache
# Usage: fs_autorecon-cw256.sh <nifti file(s)>
# Wild card can be used.
# nifti file name will be the subject id for FreeSurfer
# e.g. con001.nii -> con001
# 20 Dec 2022 K.Nemoto

#Check if the files are specified
if [ $# -lt 1 ]
then
  echo "Please specify nifti files!"
  echo "Usage: $0 <nifti_file(s)>"
  echo "Wild card can be used."
  exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done


#copy fsaverage and {lr}h.EC_average to $SUBJECTS_DIR if they don't exsit
find $SUBJECTS_DIR -maxdepth 1 | egrep fsaverage$ > /dev/null
if [ $? -eq 1 ]; then
  cp -r $FREESURFER_HOME/subjects/fsaverage $SUBJECTS_DIR
fi

find $SUBJECTS_DIR -maxdepth 1 | egrep [lr]h.EC_average$ > /dev/null
if [ $? -eq 1 ]; then
  cp -r $FREESURFER_HOME/subjects/[lr]h.EC_average $SUBJECTS_DIR
fi


#recon-all
for f in "$@"
do
  fsid=${f%.nii*}
  recon-all -i $f -s $fsid -cw256 -all -qcache
done

exit

