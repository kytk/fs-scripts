#!/usr/bin/env bash
# Script to run FreeView to check volume files
# Usage: fs_volume.sh <fsid>
# 07 Dec 2022 K.Nemoto

if [[ $# -lt 1 ]]; then
  echo "Please specify fsid!"
  echo "Usage: $0 fsid (e.g. bert)"
  exit 1
fi 

fsid=${1%/}

#Check brainmask.mgz exists
if [[ ! -f $SUBJECTS_DIR/${fsid}/mri/brainmask.mgz ]]; then
  echo "brainmask.mgz does not exist under $SUBJECTS_DIR/$fsid !"
  echo "Please check SUBJECTS_DIR !"
  exit 1
fi

freeview -v \
  $SUBJECTS_DIR/$fsid/mri/T1.mgz \
  $SUBJECTS_DIR/$fsid/mri/wm.mgz \
  $SUBJECTS_DIR/$fsid/mri/brainmask.mgz \
  $SUBJECTS_DIR/$fsid/mri/aseg.mgz:colormap=lut:opacity=0.2 \
  -f \
  $SUBJECTS_DIR/$fsid/surf/lh.white:edgecolor=blue \
  $SUBJECTS_DIR/$fsid/surf/lh.pial:edgecolor=red \
  $SUBJECTS_DIR/$fsid/surf/rh.white:edgecolor=blue \
  $SUBJECTS_DIR/$fsid/surf/rh.pial:edgecolor=red &

