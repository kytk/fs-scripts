#!/usr/bin/env bash
# view result of statistics based on mri_glmfit with all fwhm (0-25mm)
# Usage: fs_view_glm_allfhwm.sh <glmdir> <contrast> <lower threshold> <upper threshold>
# 29 Jan 2024 K.Nemoto

#set -x

#Check if the files are specified
if [[ $# -lt 4 ]]; then
  echo "Please specify glmdir, contrast file, and lower and upper thresholds!"
  echo "Usage: $0 <glmdir> <contrast> <lower threshold> <upper threshold>"
  exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

glmdir=$(echo $1 | sed -e 's@output/@@' -e 's@/$@@')
con=$(echo $2 | sed -e 's@glm/@@' -e 's@.mtx$@@')
hemi=$(echo $glmdir | cut -c 1-2)
lthresh=$3
uthresh=$4

#Confirm the parameters above before proceeding
while true
do
  echo "Parameters are the following;"
  echo "glmdir: ${glmdir}"
  echo "contrast: ${con}"
  echo "hemi: ${hemi}"
  echo "lower threshold: $3"
  echo "higher threshold: $4"
  echo "Are they correct (yes/no)?"

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

name=$(grep Title output/${glmdir}/y.fsgd | awk '{ print $2 }')

#glmdir_base (before fwhm)
gbase=${glmdir%.??.glmdir}

freeview -f \
 ${SUBJECTS_DIR}/fsaverage/surf/${hemi}.inflated:overlay=output/${gbase}.25.glmdir/${con}/sig.mgh:overlay_threshold=${lthresh},${uthresh}:name=fwhm25 \
 ${SUBJECTS_DIR}/fsaverage/surf/${hemi}.inflated:overlay=output/${gbase}.20.glmdir/${con}/sig.mgh:overlay_threshold=${lthresh},${uthresh}:name=fwhm20 \
 ${SUBJECTS_DIR}/fsaverage/surf/${hemi}.inflated:overlay=output/${gbase}.15.glmdir/${con}/sig.mgh:overlay_threshold=${lthresh},${uthresh}:name=fwhm15 \
 ${SUBJECTS_DIR}/fsaverage/surf/${hemi}.inflated:overlay=output/${gbase}.10.glmdir/${con}/sig.mgh:overlay_threshold=${lthresh},${uthresh}:name=fwhm10 \
 ${SUBJECTS_DIR}/fsaverage/surf/${hemi}.inflated:overlay=output/${gbase}.05.glmdir/${con}/sig.mgh:overlay_threshold=${lthresh},${uthresh}:name=fwhm05 \
 ${SUBJECTS_DIR}/fsaverage/surf/${hemi}.inflated:overlay=output/${gbase}.00.glmdir/${con}/sig.mgh:overlay_threshold=${lthresh},${uthresh}:name=fwhm00 \
 --layout 1 --viewport 3d &

exit

