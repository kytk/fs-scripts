#!/usr/bin/env bash
# execute mris_preproc, mri_surf2surf, and mri_glmfit based on fsgd and contrast file 
#  for local gyrification index analysis
# Prerequisite: You need to be in a directory where fsgd and contrist files 
#   exist.
# Usage: fs_glm_lgi.sh <FSGD> <contrast> 
#
# This script is based on the following paper:
# Schaer M, Cuadra MB, Schmansky N, Fischl B, Thiran JP, Eliez S. 
# How to measure cortical folding from MR images: a step-by-step tutorial
# to compute local gyrification index. J Vis Exp. 2012 Jan 2;(59):e3417.
# doi: 10.3791/3417. PMID: 22230945; PMCID: PMC3369773.
#
# 22 Aug 2021 K.Nemoto

#Check if the files are specified
if [ $# -lt 2 ]
then
  echo "Please specify FSGD, and contrast files!"
  echo "Usage: $0 <FSGD> <contrast>"
  exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

fsgd=${1}
con=$2


#Confirm fsgd and contrast before proceeding
while true
do
  echo "fsgd is ${fsgd} and contrast is ${con}."
  echo "Are they correct (yes/no)?"

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

#prepare output directory
[ ! -d output ] && mkdir output

#extract title and measurement from FSGD
title=$(grep Title ${fsgd} | awk '{ print $2 }')
meas=$(grep MeasurementName ${fsgd} | awk '{ print $2 }')

#mris_preproc
for smooth in $(seq 0 5 25)
do
  for hemi in lh rh
  do
    smoothf=$(printf "%02d" $smooth)
    mris_preproc \
      --fsgd ${fsgd} \
      --target fsaverage \
      --hemi ${hemi} \
      --meas pial_lgi \
      --out output/${hemi}.${title}.${meas}.mgh

    mri_surf2surf \
      --hemi ${hemi} \
      --s average \
      --sval output/${hemi}.${title}.${meas}.mgh \
      --fwhm ${smoothf} \
      --tval output/${hemi}.${title}.${meas}.${smoothf}.mgh

    mri_glmfit \
      --y output/${hemi}.${title}.${meas}.${smoothf}.mgh \
      --fsgd ${fsgd} doss \
      --C ${con} \
      --surf fsaverage ${hemi} \
      --glmdir output/${hemi}.${title}.${meas}.${smoothf}.glmdir
  done
done

exit

