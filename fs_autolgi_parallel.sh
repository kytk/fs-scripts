#!/usr/bin/env bash
# For-loop for LGI in parallel
# Usage: fs_autolgi_parallel.sh <FSID(s)>
# Wild card can be used.

# 24 Jul 2021 K.Nemoto

#set -x #for debugging

#Check OS
os=$(uname)

#Check number of cores (threads)
if [ $os == "Linux" ]; then
  ncores=$(nproc)
elif [ $os == "Darwin" ]; then 
  ncores=$(sysctl -n hw.ncpu)
else
  echo "Cannot detect your OS!"
  exit 1
fi

echo "Your logical cores are $ncores "

#Set parameter for parallel processing
# If $ncores >= 2, set maxrunning as $ncores - 1
# else, set maxrunning as 1

if [ $ncores -gt 1 ]; then
  maxrunning=$(($ncores - 1))
else
  maxrunning=1
fi


#Check if the files are specified
if [ $# -lt 1 ]
then
  echo "Please specify FSID"
  echo "Usage: $0 <FSID(s)>"
  echo "Wild card can be used."
  exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done


#LGI
for fsid in "$@"
do
  running=$(ps -aux | grep 'bin/recon-all' | wc -l)
  while [ $running -gt $maxrunning ];
  do
    sleep 600
    running=$(ps -aux | grep 'bin/recon-all' | wc -l)
  done
  recon-all -s $fsid -localGI &
done

exit

