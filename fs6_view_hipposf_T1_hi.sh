#!/usr/bin/env bash
# Script to view Hippocampal subfields with high resolution
# Usage: fs_view_hipposf_T1_hi.sh <fsid>
# 11 Feb 2020 K.Nemoto

if [ $# -lt 1 ]; then
  echo "Please specify fsid!"
  echo "Usage: $0 <fsid> (e.g. bert)"
  exit 1
fi 

fsid=$1

#Check if lh.hippoSfLabels-T1.v10.mgz exists
if [ ! -f $SUBJECTS_DIR/$fsid/mri/lh.hippoSfLabels-T1.v10.mgz ]; then
  echo "hippoSfLabels does not exist under $SUBJECTS_DIR/$fsid !"
  echo "Please check SUBJECTS_DIR !"
  exit 1
fi

freeview -v \
  $SUBJECTS_DIR/$fsid/mri/nu.mgz \
  $SUBJECTS_DIR/$fsid/mri/lh.hippoSfLabels-T1.v10.mgz:colormap=lut \
  $SUBJECTS_DIR/$fsid/mri/rh.hippoSfLabels-T1.v10.mgz:colormap=lut \
  --layout 1 --viewport coronal --zoom 3 &


