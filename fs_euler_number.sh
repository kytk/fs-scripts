#!/usr/bin/env bash

cd $SUBJECTS_DIR

for subj in "$@"
do 
  for h in lh rh
    do 
      echo "$subj $h $(mris_euler_number $subj/surf/$h.pial | head -n 1)"
    done
done

