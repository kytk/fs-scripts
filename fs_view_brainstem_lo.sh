#!/usr/bin/env bash
# Script to view brainstem structures with low resolution
# Usage: fs_view_brainstem_lo.sh <fsid>
# 11 Feb 2020 K.Nemoto

if [ $# -lt 1 ]; then
  echo "Please specify fsid!"
  echo "Usage: $0 <fsid> (e.g. bert)"
  exit 1
fi 

fsid=$1

#Check if brainstemSsLabels.v10.FSvoxelSpace.mgz exists
if [ ! -f $SUBJECTS_DIR/$fsid/mri/brainstemSsLabels.v10.FSvoxelSpace.mgz ]; then
  echo "brainstemSsLabels does not exist under $SUBJECTS_DIR/$fsid !"
  echo "Please check SUBJECTS_DIR !"
  exit 1
fi

freeview -v \
  $SUBJECTS_DIR/$fsid/mri/brain.mgz \
  $SUBJECTS_DIR/$fsid/mri/brainStemSsLabels.v10.FSvoxelSpace.mgz:colormap=lut \
  --layout 2 --viewport sagittal --zoom 3 &


