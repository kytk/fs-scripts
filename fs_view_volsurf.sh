#!/usr/bin/env bash
# Script to view both volume and surface data with FreeView
# Usage: fs_volsurf.sh <fsid>
# 07 Dec 2022 K.Nemoto

if [[ $# -lt 1 ]]; then
  echo "Please specify fsid!"
  echo "Usage: $0 fsid (e.g. bert)"
  exit 1
fi 

fsid=${1%/}

#Check brainmask.mgz exists
if [[ ! -f ${SUBJECTS_DIR}/${fsid}/mri/brainmask.mgz ]]; then
  echo "brainmask.mgz does not exist under ${SUBJECTS_DIR}/${fsid} !"
  echo "Please check SUBJECTS_DIR !"
  exit 1
fi


freeview -v \
  ${SUBJECTS_DIR}/${fsid}/mri/T1.mgz \
  ${SUBJECTS_DIR}/${fsid}/mri/wm.mgz \
  ${SUBJECTS_DIR}/${fsid}/mri/brainmask.mgz \
  ${SUBJECTS_DIR}/${fsid}/mri/aseg.mgz:colormap=lut:opacity=0.2 \
  -f \
  ${SUBJECTS_DIR}/${fsid}/surf/lh.pial:annot=aparc.annot:name=lh.pial_aparc:visible=0 \
  ${SUBJECTS_DIR}/${fsid}/surf/rh.pial:annot=aparc.annot:name=rh.pial_aparc:visible=0 \
  ${SUBJECTS_DIR}/${fsid}/surf/lh.inflated:overlay=lh.thickness:overlay_threshold=0.1,3::name=lh.inflated_thickness:visible=0 \
  ${SUBJECTS_DIR}/${fsid}/surf/rh.inflated:overlay=rh.thickness:overlay_threshold=0.1,3::name=rh.inflated_thickness:visible=0 \
  ${SUBJECTS_DIR}/${fsid}/surf/lh.inflated:visible=0 \
  ${SUBJECTS_DIR}/${fsid}/surf/rh.inflated:visible=0 \
  ${SUBJECTS_DIR}/${fsid}/surf/lh.white:visible=0 \
  ${SUBJECTS_DIR}/${fsid}/surf/rh.white:visible=0 \
  ${SUBJECTS_DIR}/${fsid}/surf/lh.pial \
  ${SUBJECTS_DIR}/${fsid}/surf/rh.pial \
  --viewport 3d &
