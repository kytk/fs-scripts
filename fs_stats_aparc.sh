#!/usr/bin/env bash
# Script to utilize aparcstats2table
# Surface, Thickness, Thickness_SD, and Volume csv files will be generated.
# Usage: fs_starts_aparc.sh <subject_id(s)>
# 07 Nov 2020 K.Nemoto

#set -x

timestamp=$(date +%Y%m%d_%H%M)

#Check if the fsid(s) are specified
if [ $# -lt 1 ] ; then
  echo "Please specify fsid(s) you want to extract ROI!"
  echo "Usage: $0 <fsid(s)>"
  exit 1
fi

#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

cd $SUBJECTS_DIR

#Check if tables directory exists in $SUBJECTS_DIR
if [[ ! -d tables ]]; then
  mkdir tables
fi
cd tables


# Atlas: aparc, aparc.a2009s, aparc.DKTatlas
# hemi: lh, rh
# measure: area, volume, thickness, thicknessstd, meancurv

for atlas in aparc aparc.a2009s aparc.DKTatlas
do
  for hemi in lh rh
  do
    for measure in area volume thickness thicknessstd meancurv
    do
    aparcstats2table --hemi ${hemi} \
      --subjects "$@" \
      --delimiter comma \
      --measure ${measure} \
      --parc ${atlas} \
      --table ${timestamp}.${hemi}.${atlas}.${measure}.csv

    #transpose
    aparcstats2table --hemi ${hemi} \
      --subjects "$@" \
      --delimiter comma \
      --measure ${measure} \
      --parc ${atlas} \
      --transpose \
      --table ${timestamp}.${hemi}.${atlas}.${measure}_trans.csv

    done
  done
done


####right hemisphere
#aparcstats2table --hemi rh \
#  --subjects "$@" \
#  --delimiter comma \
#  --parc aparc.a2009s \
#  --table ${timestamp}.rh.aparc.Destrieux.area.csv
#
###Destrieux-Thickness
####left hemisphere
#aparcstats2table --hemi lh \
#  --subjects "$@" \
#  --meas thickness \
#  --delimiter comma \
#  --parc aparc.a2009s \
#  --table ${timestamp}.lh.aparc.Destrieux.thickness.csv
#
####right hemisphere
#aparcstats2table --hemi rh \
#  --subjects "$@" \
#  --meas thickness \
#  --delimiter comma \
#  --parc aparc.a2009s \
#  --table ${timestamp}.rh.aparc.Destrieux.thickness.csv
#
###Destrieux-Thicknessstd
####left hemisphere
#aparcstats2table --hemi lh \
#  --subjects "$@" \
#  --meas thicknessstd \
#  --delimiter comma \
#  --parc aparc.a2009s \
#  --table ${timestamp}.lh.aparc.Destrieux.thicknessstd.csv
#
####right hemisphere
#aparcstats2table --hemi rh \
#  --subjects "$@" \
#  --meas thicknessstd \
#  --delimiter comma \
#  --parc aparc.a2009s \
#  --table ${timestamp}.rh.aparc.Destrieux.thicknessstd.csv
#
###Destrieux-Volume
####left hemisphere
#aparcstats2table --hemi lh \
#  --subjects "$@" \
#  --meas volume \
#  --delimiter comma \
#  --parc aparc.a2009s \
#  --table ${timestamp}.lh.aparc.Destrieux.volume.csv
#
####right hemisphere
#aparcstats2table --hemi rh \
#  --subjects "$@" \
#  --meas volume \
#  --delimiter comma \
#  --parc aparc.a2009s \
#  --table ${timestamp}.rh.aparc.Destrieux.volume.csv
#
#
##DKT
###Area
####left hemisphere
#aparcstats2table --hemi lh \
#  --subjects "$@" \
#  --delimiter comma \
#  --parc aparc.DKTatlas \
#  --table ${timestamp}.lh.aparc.DKT.area.csv
#
####right hemisphere
#aparcstats2table --hemi rh \
#  --subjects "$@" \
#  --delimiter comma \
#  --parc aparc.DKTatlas \
#  --table ${timestamp}.rh.aparc.DKT.area.csv
#
###DKT-Thickness
####left hemisphere
#aparcstats2table --hemi lh \
#  --subjects "$@" \
#  --meas thickness \
#  --delimiter comma \
#  --parc aparc.DKTatlas \
#  --table ${timestamp}.lh.aparc.DKT.thickness.csv
#
####right hemisphere
#aparcstats2table --hemi rh \
#  --subjects "$@" \
#  --meas thickness \
#  --delimiter comma \
#  --parc aparc.DKTatlas \
#  --table ${timestamp}.rh.aparc.DKT.thickness.csv
#
###DKT-Thicknessstd
####left hemisphere
#aparcstats2table --hemi lh \
#  --subjects "$@" \
#  --meas thicknessstd \
#  --delimiter comma \
#  --parc aparc.DKTatlas \
#  --table ${timestamp}.lh.aparc.DKT.thicknessstd.csv
#
####right hemisphere
#aparcstats2table --hemi rh \
#  --subjects "$@" \
#  --meas thicknessstd \
#  --delimiter comma \
#  --parc aparc.DKTatlas \
#  --table ${timestamp}.rh.aparc.DKT.thicknessstd.csv
#
###DKT-Volume
####left hemisphere
#aparcstats2table --hemi lh \
#  --subjects "$@" \
#  --meas volume \
#  --delimiter comma \
#  --parc aparc.DKTatlas \
#  --table ${timestamp}.lh.aparc.DKT.volume.csv
#
####right hemisphere
#aparcstats2table --hemi rh \
#  --subjects "$@" \
#  --meas volume \
#  --delimiter comma \
#  --parc aparc.DKTatlas \
#  --table ${timestamp}.rh.aparc.DKT.volume.csv

cd $SUBJECTS_DIR

echo "CSV files were generated! Please check 'tables' directory"

exit

