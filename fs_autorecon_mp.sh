#!/usr/bin/env bash
# For-loop for recon-all with mp settings
# fs_autorecon_parallel is more efficient and preferred.
# Usage: fs_autorecon_mp.sh <nifti file(s)>
# Wild card can be used
# 11 Feb 2020 K.Nemoto

#Check OS
os=$(uname)

#Check number of cores (threads)
if [ $os == "Linux" ]; then
  ncores=$(nproc)
elif [ $os == "Darwin" ]; then 
  ncores=$(sysctl -n hw.ncpu)
else
  echo "Cannot detect your OS!"
  exit 1
fi

echo "Your logical cores are ${ncores}"

#If logical cores >=4, enable parallelize option
if [ $ncores -ge 4 ]; then
  parallel="-parallel"
  echo "enable parallel mode"
else
  parallel=""
  echo "disable parallel mode"
fi

#If logical cores >=8, enable openmp option
if [ $ncores -ge 8 ]; then
  fsncores=$(($ncores-2))
  mp="-openmp $fsncores"
else
  mp=""
fi

#Check if the files are specified
if [ $# -lt 1 ]
then
  echo "Please specify nifti files!"
  echo "Usage: $0 <nifti_file(s)>"
  echo "Wild card can be used."
  exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done


# recon-all
for f in "$@"
do
  fsid=${f%.nii*}
  recon-all -i $f -s $fsid -all $parallel $mp
done

exit

