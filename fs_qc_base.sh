#!/usr/bin/env bash
# Script for QC of longitudinal base with FreeView
# Usage: fs_qc_base.sh <base fsid>
# 07 Jan 2020 K.Nemoto

if [ $# -lt 1 ]; then
  echo "Please specify base fsid!"
  echo "Usage: $0 <fsid> (e.g. bert)"
  exit 1
fi 


#Display $SUBJECTS_DIR
echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"

#Set fsid from argument
fsid=${1%/}

cd $SUBJECTS_DIR

#Check norm.mgz exists
if [ ! -f $SUBJECTS_DIR/$fsid/mri/norm.mgz ]; then
  echo "norm.mgz does not exist!"
  echo "It seems recon-all exited with error!"
  exit 1
fi

freeview -v $fsid/mri/norm.mgz \
 -f $fsid/surf/lh.pial:edgecolor=red \
    $fsid/surf/rh.pial:edgecolor=red \
    $fsid/surf/lh.white:edgecolor=blue \
    $fsid/surf/rh.white:edgecolor=blue \
 --layout 1 --viewport coronal --zoom 2 &

 
