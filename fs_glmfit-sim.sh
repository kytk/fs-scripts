#!/usr/bin/env bash
# execute mri_glmfit-sim
# Usage: fs_glmfit-sim.sh <glmdir> <Cluster-defining threshold> 
# 25 Feb 2020 K.Nemoto

#Check if the files are specified
if [ $# -lt 2 ]
then
  echo "Please specify glmdir and cluster-defining threshold (CDT)!"
  echo "Usage: $0 <glmdir> <CDT>"
  exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

glmdir=${1%/}
cdt=$2


#Confirm fsgd and contrast before proceeding
while true
do
  echo "glmdir is ${glmdir} and CDT is ${cdt}."
  echo "Are they correct (yes/no)?"

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

#mri_glmfit-sim
for side in pos neg abs
do
  mri_glmfit-sim --glmdir ${glmdir} \
    --cache ${cdt} ${side} \
    --cwp 0.05 --2spaces
done

exit

