#!/usr/bin/env bash
# execute mris_preproc and mri_glmfit based on fsgd and contrast file
# Prerequisite: You need to be in a directory where fsgd and contrist files 
#   exist.
# Usage: fs_glm.sh <FSGD> <contrast> 
# 08 Nov 2020 K.Nemoto

#Check if the files are specified
if [ $# -lt 2 ]
then
  echo "Please specify FSGD, and contrast files!"
  echo "Usage: $0 <FSGD> <contrast>"
  exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

fsgd=$1
con=$2


#Confirm fsgd and contrast before proceeding
while true
do
  echo "fsgd is ${fsgd} and contrast is ${con}."
  echo "Are they correct (yes/no)?"

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

#prepare output directory
[ ! -d output ] && mkdir output

#extract title and measurement from FSGD
title=$(grep Title ${fsgd} | awk '{ print $2 }')
meas=$(grep MeasurementName ${fsgd} | awk '{ print $2 }')

#mris_preproc
for smooth in $(seq 0 5 25)
do
  for hemi in lh rh
  do
    smoothf=$(printf "%02d" $smooth)
    mris_preproc \
      --fsgd ${fsgd} \
      --cache-in ${meas}.fwhm${smooth}.fsaverage \
      --target fsaverage \
      --hemi ${hemi} \
      --out output/${hemi}.${title}.${meas}.${smoothf}.mgh

    mri_glmfit \
      --y output/${hemi}.${title}.${meas}.${smoothf}.mgh \
      --fsgd ${fsgd} dods \
      --C ${con} \
      --surf fsaverage ${hemi} \
      --cortex \
      --glmdir output/${hemi}.${title}.${meas}.${smoothf}.glmdir \
      --eres-save
  done
done

exit

