#!/usr/bin/env bash
# For-loop for hippocampal subfields and amygdala with only T1
# Requirements: recon-all should be finished.
# Usage: fs_autoHA_T1_long.sh <base_fsid(s)>
# Wildcard can be used.
# 11 Feb 2020 K.Nemoto

#Check if the fsid(s) are specified
if [ $# -lt 1 ]
then
	echo "Please specify base_fsid(s) to specify subjects!"
	echo "Usage: $0 <base_fsid(s)>"
	exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done


#segmentHA_T1
for f in "$@"
do
  fsid=${f%/}
  echo "Begin longitudinal hippocampal subfield and amygdala segmentation of $fsid"
  segmentHA_T1_long.sh $fsid
done

echo "Longitudinal hippocampal subfield and amygdala segmentation finished!"

exit

