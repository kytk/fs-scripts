#!/usr/bin/env bash

# Script to count number of fsid recon-all finished without error
# under $SUBJECTS_DIR
# K. Nemoto 3 May 2024

# Print date
date

# Print the number of FSID without error
echo "Number of subjects without recon-all hard error under SUBJECTS_DIR"
echo "SUBJECTS_DIR is $SUBJECTS_DIR"
for file in $(find $SUBJECTS_DIR -name 'recon-all-status.log')
do 
  tail -n 1 $file | grep 'without error'
done | wc -l

