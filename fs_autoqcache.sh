#!/usr/bin/env bash
# For-loop for recon-all -qcache
# Usage: fs_autoqcache.sh <fsid(s)>
# Wild card can be used.
# 11 Feb 2020 K.Nemoto

#set -x

#Check if the fsid(s) are specified
if [ $# -lt 1 ]
then
	echo "Please specify fsid(s)!"
	echo "Usage: $0 <fsid(s)>"
	echo "Wild card can be used."
	exit 1
fi


#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done


#copy fsaverage and {lr}h.EC_average to $SUBJECTS_DIR if they don't exsit
find $SUBJECTS_DIR -maxdepth 1 | egrep fsaverage$ > /dev/null
if [ $? -eq 1 ]; then
  cp -r $FREESURFER_HOME/subjects/fsaverage $SUBJECTS_DIR
fi

find $SUBJECTS_DIR -maxdepth 1 | egrep [lr]h.EC_average$ > /dev/null
if [ $? -eq 1 ]; then
  cp -r $FREESURFER_HOME/subjects/[lr]h.EC_average $SUBJECTS_DIR
fi


# recon-all -qcache
for f in "$@"
do
  fsid=${f%/}    
  recon-all -s $fsid -qcache
done

exit

