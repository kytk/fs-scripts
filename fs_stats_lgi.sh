#!/usr/bin/env bash
# Script to utilize mri_segstats
# Usage: fs_starts_lgi.sh <subject_id(s)>
# 08 Aug 2021 K.Nemoto

#set -x

timestamp=$(date +%Y%m%d_%H%M)

#Check if the fsid(s) are specified
if [ $# -lt 1 ] ; then
  echo "Please specify fsid(s) you want to extract ROI!"
  echo "Usage: $0 <fsid(s)>"
  exit 1
fi

#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

cd $SUBJECTS_DIR

for fsid in "$@"
do
  #Desikan-Killiany
  ###left hemisphere
  mri_segstats --annot $fsid lh aparc \
    --i $SUBJECTS_DIR/$fsid/surf/lh.pial_lgi \
    --sum $SUBJECTS_DIR/$fsid/stats/lh.aparc.pial_lgi.stats
  cat $SUBJECTS_DIR/$fsid/stats/lh.aparc.pial_lgi.stats | \
    sed -e 's/# subjectname/subjectname/' -e 's/# ColHeaders  *Index/Index/' \
        -e 's/^  *//' -e 's/  */,/g' | 
    grep -v "#" > $SUBJECTS_DIR/$fsid/stats/lh.aparc.pial_lgi.stats.csv  

  ###right hemisphere
  mri_segstats --annot $fsid rh aparc \
    --i $SUBJECTS_DIR/$fsid/surf/rh.pial_lgi \
    --sum $SUBJECTS_DIR/$fsid/stats/rh.aparc.pial_lgi.stats
  cat $SUBJECTS_DIR/$fsid/stats/rh.aparc.pial_lgi.stats | \
    sed -e 's/# subjectname/subjectname/' -e 's/# ColHeaders  *Index/Index/' \
        -e 's/^  *//' -e 's/  */,/g' | 
    grep -v "#" > $SUBJECTS_DIR/$fsid/stats/rh.aparc.pial_lgi.stats.csv  
done

exit

