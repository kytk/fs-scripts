#!/usr/bin/env bash
# Script to generate aparc roi files
# Roi files were made from aparc+aseg.mgz in fsaverage/mri.

# 05 Feb 2022 K.Nemoto

cd $SUBJECTS_DIR
mkdir aparc_roi
cd aparc_roi

# Copy aparc+aseg.mgz from fsaverage/mri
cp $SUBJECTS_DIR/fsaverage/mri/aparc+aseg.mgz .

mri_label2vol --seg aparc+aseg.mgz --temp aparc+aseg.mgz --o tmp.mgz --identity

mri_convert tmp.mgz tmp.nii.gz --out_orientation LAS

# Left hemisphere
for f in $(seq 1001 1035)
  do fslmaths tmp.nii.gz -thr $f -uthr $f -bin aparc_${f}
done

# Right hemisphere
for f in $(seq 2001 2035)
  do fslmaths tmp.nii.gz -thr $f -uthr $f -bin aparc_${f}
done

# Remove corpus callosum
rm aparc_1004.nii.gz aparc_2004.nii.gz

# Delete temporary files
rm tmp.{mgz,nii.gz} aparc+aseg.mgz



