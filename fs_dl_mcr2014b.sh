#!/usr/bin/env bash
# Script to set Matlab R2014b Runtime for hippocampal subfield calculation
# This script is based on 
# https://surfer.nmr.mgh.harvard.edu/fswiki/MatlabRuntime
# 26 Sep 2020 K.Nemoto

sudo FREESURFER_HOME=$FREESURFER_HOME $FREESURFER_HOME/bin/fs_install_mcr R2014b

exit

