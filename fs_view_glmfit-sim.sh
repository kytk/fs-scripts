#!/usr/bin/env bash
# view result of statistics based on mri_glmfit 
# Usage: fs_view_glmfit-sim.sh <glmdir> <con> <CDT> <pos/neg/abs>
# 02 Feb 2025 K.Nemoto

#set -x

#Check if the files are specified
if [[ $# -lt 4 ]]; then
  echo "Please specify glmdir, contrast file, and lower and upper thresholds!"
  echo "Usage: $0 <glmdir> <contrast> <CDT> <pos/neg/abs>"
  echo "This script can visualize both standard and permutation-based cluster-wise correction results."
  echo "You will be asked to choose the statistical method during execution."
  exit 1
fi

#Display $SUBJECTS_DIR and ask to proceed
while true
do
  echo "Your current SUBJECTS_DIR is $SUBJECTS_DIR"
  echo "Do you want to proceed (yes/no)? "

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

glmdir=$(echo $1 | sed -e 's@output/@@' -e 's@/$@@')
con=$(echo $2 | sed -e 's@glm/@@' -e 's@.mtx$@@')
hemi=$(echo $glmdir | cut -c 1-2)
cdt=$(echo $3 | awk '{ printf("%d", $1*10) '})
side=$4

#Confirm glmdir, contrast, and cdt before proceeding
while true
do
  echo "Parameters are the following;"
  echo "glmdir: ${glmdir}"
  echo "contrast: ${con}"
  echo "hemi: ${hemi}"
  echo "CDT*10: ${cdt}"
  echo "side: ${side}"
  echo "Are they correct (yes/no)?"

  read answer

  case $answer in
    [Yy]*)
      break
      ;;
    [Nn]*)
      echo -e "Abort. \n"
      exit
      ;;
    *)
      echo -e "Type yes or no.\n"
      ;;
  esac
done

#Ask which statistical method to use
while true
do
  echo "Which statistical method do you want to use?"
  echo "1) Standard cluster-wise correction (cache)"
  echo "2) Permutation-based correction (perm)"
  echo "Please enter 1 or 2: "

  read stat_method

  case $stat_method in
    1)
      prefix="cache"
      break
      ;;
    2)
      prefix="perm"
      break
      ;;
    *)
      echo -e "Please enter 1 or 2.\n"
      ;;
  esac
done

# Define paths for required files
cluster=output/${glmdir}/${con}/${prefix}.th${cdt}.${side}.sig.cluster.mgh
annot=output/${glmdir}/${con}/${prefix}.th${cdt}.${side}.sig.ocn.annot

# Check if required files exist
if [ ! -f "$cluster" ]; then
  echo "Error: $cluster not found"
  exit 1
fi

if [ ! -f "$annot" ]; then
  echo "Error: $annot not found"
  exit 1
fi

# Launch freeview with the specified parameters
freeview -f \
$SUBJECTS_DIR/fsaverage/surf/${hemi}.inflated:\
overlay=${cluster}:\
annot=${annot}:\
name=${hemi}.${side}.corrected \
-layout 1 -viewport 3d &

exit
